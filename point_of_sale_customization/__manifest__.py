# -*- coding: utf-8 -*-
{
    "name": "POS Customization",
    'summary': 'POS Customization',
    'description': """POS Customization""",
    "depends": ['point_of_sale'],
    "data": [
        'security/ir_rule.xml',
        'views/pos_payment_views.xml'
    ],
}
