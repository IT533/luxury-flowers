from odoo import api, fields, models, tools, _


class AccountMoveInherit(models.Model):
    _inherit = "account.move"

    payment_order_reference = fields.Char(string='Payment Order', index=True, copy=False)


class PosOrderInherit(models.Model):
    _inherit = "pos.order"

    def _prepare_invoice_vals(self):
        res = super(PosOrderInherit, self)._prepare_invoice_vals()
        res['payment_order_reference'] = ' '.join([str(x.payment_method_id.name) for x in self.payment_ids if x.amount > 0])
        return res
