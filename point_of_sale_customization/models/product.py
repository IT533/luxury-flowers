from odoo import api, models, fields


class ProductInherit(models.Model):
    _inherit = "product.product"

    arabic_name = fields.Char(string='Arabic Name')
