# -*- coding: utf-8 -*-
"""Customer Collection Report"""
from odoo import api, models


class ReportRender(models.AbstractModel):
    """Customer Collection Report"""
    _name = 'report.collection_report.collection_report_template'

    def _lines(self, data):
        """Customer Collection Report get the values and return back to print function"""
        if data['user_ids'] and data['partner_ids']:
            if data['date_from']:
                filter = "( ap.collection = True and ap.payment_date >= %s" \
                         "AND ap.payment_date <= %s AND ap.company_id=%s AND ai.invoice_user_id = %s" \
                         "AND ap.partner_id = %s AND ap.amount > 0)"

                arg_list = (data['date_from'], data['date_to'], data['company_id'],
                            data['user_ids'], data['partner_ids'],)
            else:
                filter = "( ap.collection = True and ai.invoice_user_id = %s" \
                         " AND ap.payment_date <= %s AND ap.company_id=%s" \
                         "AND ap.partner_id = %s AND ap.amount > 0)"

                arg_list = (data['user_ids'], data['date_to'], data['company_id'], data['partner_ids'],)

        elif not data['user_ids'] and data['partner_ids']:
            if data['date_from']:
                filter = "( ap.collection = True and ap.payment_date >= %s" \
                         "AND ap.payment_date <= %s AND ap.company_id=%s" \
                         "AND ap.partner_id = %s AND ap.amount > 0)"

                arg_list = (data['date_from'], data['date_to'], data['company_id'],
                            data['partner_ids'],)
            else:
                filter = "( ap.collection = True and ap.state = 'posted'" \
                         " AND ap.payment_date <= %s AND ap.company_id=%s" \
                         " AND ap.partner_id = %s AND ap.amount > 0)"

                arg_list = (data['date_to'], data['company_id'], data['partner_ids'],)

        elif data['user_ids'] and not data['partner_ids']:
            if data['date_from']:
                filter = "( ap.collection = True and ap.payment_date >= %s" \
                         "AND ap.payment_date <= %s AND ap.company_id=%s AND ai.invoice_user_id = %s" \
                         "AND ap.amount > 0)"

                arg_list = (data['date_from'], data['date_to'], data['company_id'],
                            data['user_ids'],)
            else:
                filter = "( ap.collection = True and ai.invoice_user_id = %s" \
                         " AND ap.payment_date <= %s AND ap.company_id=%s" \
                         "AND ap.amount > 0)"

                arg_list = (data['user_ids'], data['date_to'], data['company_id'],)

        elif not data['user_ids'] and not data['partner_ids']:
            if data['date_from']:
                filter = "( ap.collection = True and ap.payment_date >= %s" \
                         "AND ap.payment_date <= %s AND ap.company_id=%s " \
                         "AND ap.amount > 0)"

                arg_list = (data['date_from'], data['date_to'], data['company_id'],)
            else:
                filter = "( ap.collection = True" \
                         " AND ap.payment_date <= %s AND ap.company_id=%s" \
                         "AND ap.amount > 0)"

                arg_list = (data['date_to'], data['company_id'],)

        else:
            if data['date_from']:
                filter = "( ap.collection = True and ap.payment_date >= %s" \
                         " AND ap.payment_date <=%s AND ap.company_id=%s AND ap.amount > 0)"

                arg_list = (data['date_from'], data['date_to'], data['company_id'])
            else:
                filter = "( ap.collection = True" \
                         "AND ap.payment_date <= %s AND ap.company_id=%s AND ap.amount > 0)"

                arg_list = (data['date_to'], data['company_id'],)

        self._cr.execute("""SELECT rp.name AS partner, rp.uniqueid AS code, apr.amount AS payment,
                            ai.amount_total as total, ap.amount as p_am,
                            aml.date AS p_date, aj.type AS p_journal, amml.name AS a_name, ai.number AS invoice,ai.residual,
                            ai.amount_total as total, ai.date_invoice as invoice_date, aml.credit as c_p
                            FROM account_partial_reconcile apr
                            INNER JOIN account_move_line aml ON aml.id = apr.credit_move_id
                            INNER JOIN res_partner rp ON rp.id= aml.partner_id
                            INNER JOIN account_move am ON am.id=aml.move_id
                            INNER JOIN account_journal aj ON aj.id=am.journal_id
                            INNER JOIN account_payment ap ON ap.id =aml.payment_id
                            LEFT JOIN account_move_line amml ON amml.id=apr.debit_move_id
                            LEFT JOIN account_invoice ai ON ai.id = amml.invoice_id
                            WHERE """ + filter + """
                                """, arg_list)
        record = self._cr.dictfetchall()
        for i in record:
            if i['p_am'] != i['c_p']:
                if i['p_journal'] == 'cash':
                    i['c_payment'] = i['p_am']
                    i['b_payment'] = 0.0
                    i['disc'] = i['c_p'] - i['p_am']
                else:
                    i['b_payment'] = i['p_am']
                    i['c_payment'] = 0.0
                    i['disc'] = round(i['c_p'] - i['p_am'], 2)
            else:
                if i['p_journal'] == 'cash':
                    i['c_payment'] = i['payment']
                    i['b_payment'] = 0.0
                    i['disc'] = 0.0
                else:
                    i['b_payment'] = i['payment']
                    i['c_payment'] = 0.0
                    i['disc'] = 0.0
        return record

    def get_report_values(self, docid, data):
        """Retreive the values and pass to the report xml"""
        lines = self._lines(data)
        return {
            'data': data,
            'lines': lines
        }
