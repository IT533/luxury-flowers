# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import api, models, _


class ReportRender(models.AbstractModel):
    _name = 'report.collection_report.partner_collection_report'

    def _get_partner_balance(self, data):

        if data['user_id']:
            filter = "(aml.date < %s and aat.type ='receivable' AND aml.company_id=%s AND ai.invoice_user_id=%s )"

            arg_list = (data['date'], data['company_id'][0], data['user_id'][0],)
        else:
            filter = "(aml.date < %s and aat.type ='receivable' AND aml.company_id=%s)"

            arg_list = (data['date'], data['company_id'][0],)
        self.env.cr.execute("""SELECT rp.id as id, rp.name as partner, sum(aml.debit- aml.credit) as balance, rp.uniqueid as code
                    from account_move_line aml
                    inner join res_partner rp on rp.id=aml.partner_id
                    inner join account_invoice ai on ai.id=aml.invoice_id
                    inner join account_account aa on aa.id=aml.account_id
                    inner join account_account_type aat on aat.id=aa.user_type_id
                    where """ + filter + """
                    group by rp.name, rp.uniqueid, rp.id""", arg_list)
        record = self.env.cr.dictfetchall()
        for i in record:
            if data['user_id']:
                self._cr.execute("""select aj.name as payment, sum(absl.amount) as amount
                            from pos_order po
                            inner join account_bank_statement_line absl on absl.pos_statement_id = po.id
                            inner join res_partner rp on rp.id = po.partner_id
                            inner join account_journal aj on aj.id = absl.journal_id
                            WHERE rp.id=%s AND po.company_id=%s AND absl.date=%s AND po.user_id=%s
                            group by rp.name, aj.name """,
                                 (i['id'], data['company_id'][0], data['date'], data['user_id'][0]))
            else:
                self._cr.execute("""select aj.name as payment, sum(absl.amount) as amount
                            from pos_order po
                            inner join account_bank_statement_line absl on absl.pos_statement_id = po.id
                            inner join res_partner rp on rp.id = po.partner_id
                            inner join account_journal aj on aj.id = absl.journal_id
                            WHERE rp.id=%s AND po.company_id=%s AND absl.date=%s
                            group by rp.name, aj.name """, (i['id'], data['company_id'][0], data['date']))

            daily_sales = self._cr.dictfetchall()

            self._cr.execute("""select sum(credit) as collection, aj.type as payment
                    from account_move_line aml
                    inner join account_payment ap on ap.id=aml.payment_id
                    inner join res_partner rp on rp.id=aml.partner_id
                    inner join account_account aa on aa.id=aml.account_id
                    inner join account_account_type aat on aat.id=aa.user_type_id
                    inner join account_journal aj on aj.id = ap.journal_id
                    where aml.date = %s and aat.type ='receivable' and ap.collection=True
                    AND ap.company_id=%s AND rp.id=%s
                    group by rp.name, aj.type
                    """, (data['date'], data['company_id'][0], i['id']))
            collection = self._cr.dictfetchall()
            if collection:
                for rec in collection:
                    if rec:
                        if rec['payment'] == 'cash':
                            i['cash'] = rec['collection']
                            i['bank'] = 0
                        elif rec['payment'] == 'bank':
                            i['bank'] = rec['collection']
                            i['cash'] = 0
                        else:
                            i['cash'] = 0
                            i['bank'] = 0
            else:
                i['cash'] = 0
                i['bank'] = 0
            if daily_sales:
                for rec in daily_sales:
                    if rec:
                        if rec['payment'] == 'Cash':
                            i['d_cash'] = rec['amount']
                            i['d_bank'] = 0
                            i['credit'] = 0
                        elif rec['payment'] == 'Debt':
                            i['credit'] = rec['amount']
                            i['d_cash'] = 0
                            i['d_bank'] = 0
                        else:
                            i['d_cash'] = 0
                            i['d_bank'] = rec['amount']
                            i['credit'] = 0
            else:
                i['d_cash'] = 0
                i['d_bank'] = 0
                i['credit'] = 0
        return record

    def get_report_values(self, docid, data):
        lines = self._get_partner_balance(data['form'])
        return {
            'data': data,
            'lines': lines,
        }
