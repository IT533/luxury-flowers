# -*- coding: utf-8 -*-
from datetime import datetime
import logging
from odoo import models, api
import itertools



class CollectionReport(models.AbstractModel):
    _name = 'report.collection_report.collection_report_xlsx'
    _logger = logging.getLogger(__name__)
    try:
        _inherit = 'report.report_xlsx.abstract'
    except ImportError:
        _logger.debug('Cannot find report_xlsx module for version 11')

    def _lines(self, data):
        if data.user_ids.id and not data.partner_ids.id:
            if data.date_from:
                # (line.date >= % s " \
                #                          "AND line.date <= % s AND  move.company_id= % s AND invoice.invoice_user_id = % s " \
                #                          "AND
                filter = "move.date >= %s " \
                         "AND move.date <= %s AND invoice.invoice_user_id = %s "
                arg_list = (data.date_from, data.date_to,
                            data.user_ids.id,)
            else:
                filter = "(invoice.invoice_user_id = %s " \
                         " AND move.date <= %s AND move.company_id=%s" \
                         "AND payment.amount > 0)"

                arg_list = (data.user_ids.id, data.date_to, data.company_id.id,)

        elif not data.user_ids.id and not data.partner_ids.id:
            if data.date_from:
                filter = "(invoice.invoice_date >= %s " \
                         "AND move.date <= %s AND move.company_id=%s " \
                         "AND payment.amount > 0)"

                arg_list = (data.date_from, data.date_to, data.company_id.id,)
            else:
                filter = "(invoice.invoice_date <= %s AND move.company_id=%s" \
                         "AND payment.amount > 0)"

                arg_list = (data.date_to, data.company_id.id,)

        else:
            if data.date_from:
                filter = "(invoice.invoice_date >= %s " \
                         " AND invoice.invoice_date <=%s AND move.company_id=%s AND payment.amount > 0)"

                arg_list = (data.date_from, data.date_to, data.company_id.id)
            else:
                filter = "(invoice.invoice_date <= %s AND move.company_id=%s AND payment.amount > 0)"

                arg_list = (data.date_to, data.company_id.id,)
        query = """
             SELECT
              rp.name AS partner, payment.amount AS p_amount,
            invoice.amount_total as total, move.amount_total as p_am,
             move.date AS p_date, aj.name AS ref, line.name AS a_name,
            invoice.name AS invoice_ref,invoice.amount_residual,
             invoice.invoice_date as invoice_date, line.credit as c_p
            FROM account_payment payment
            JOIN account_move move ON move.id = payment.move_id
            JOIN account_journal aj ON aj.id = move.journal_id
            JOIN account_move_line line ON line.move_id = move.id
            JOIN account_partial_reconcile part ON
                part.debit_move_id = line.id
                OR
                part.credit_move_id = line.id
            JOIN account_move_line counterpart_line ON
                part.debit_move_id = counterpart_line.id
                OR
                part.credit_move_id = counterpart_line.id
            JOIN account_move invoice ON invoice.id = counterpart_line.move_id
            JOIN res_partner rp ON rp.id= invoice.partner_id
            WHERE invoice.payment_order_reference LIKE 'Debt' 
            AND
         """ + filter + """"""
        self._cr.execute(query, arg_list)
        record = self._cr.dictfetchall()
        return record

    def generate_xlsx_report(self, workbook, records, data):
        sheet = workbook.add_worksheet()
        format1 = workbook.add_format({'font_size': 14, 'font_color': '#725169', 'bold': True, 'align': 'center'})
        format2 = workbook.add_format({'font_size': 10, 'bold': True})
        format3 = workbook.add_format({'font_size': 10})
        format4 = workbook.add_format({'font_size': 10})
        currency = self.env.user.company_id.currency_id.symbol
        format4.set_num_format('0.00 ' + currency)
        row_value = 3
        lines = self._lines(data)
        sheet.write(row_value, 0, "Date From", format2)
        date_from = datetime.strptime(str(data.date_from), "%Y-%m-%d").date()
        sheet.write(row_value, 1, date_from.strftime("%d/%b/%Y"), format3)
        row_value += 1
        if data.date_to:
            date_to = datetime.strptime(str(data.date_to), "%Y-%m-%d").date()
            sheet.write(row_value, 0, "Date To", format2)
            sheet.write(row_value, 1, date_to.strftime("%d/%b/%Y"), format3)
            row_value += 1
        row_value += 1
        sheet.set_column(0, 0, 5)
        sheet.set_column(0, 1, 15)
        sheet.set_column(0, 2, 15)
        sheet.set_column(0, 3, 10)
        sheet.set_column(0, 4, 15)
        sheet.merge_range('A2:H2', "Collection Report ", format1)
        sheet.write(row_value, 0, "Sl No", format2)
        sheet.write(row_value, 1, "Customer Name", format2)
        sheet.write(row_value, 2, "Invoice", format2)
        sheet.write(row_value, 3, "Invoice Date", format2)
        sheet.write(row_value, 4, "Payment Date", format2)
        sheet.write(row_value, 5, "Invoice Total", format2)
        sheet.write(row_value, 6, "Cash", format2)
        sheet.write(row_value, 7, "Bank", format2)
        sheet.write(row_value, 8, "Balance", format2)
        row_value += 1
        total = 0
        cash_total = 0
        bank_total = 0
        balance = 0
        sl = 1
        list_invoice = []
        for items in lines:
            payment_date = datetime.strptime(str(items['p_date']), "%Y-%m-%d").date()
            invoice_date = items['invoice_date'] and datetime.strptime(str(items['invoice_date']),
                                                                       "%Y-%m-%d").date() or ''
            invoice_date = invoice_date and invoice_date.strftime("%d/%b/%Y") or ''
            if items['p_amount'] > items['total']:
                items['p_amount'] = items['total']
            bank_payment = items['ref'] != 'Cash' and items['p_amount'] or 0
            cash_payment = items['ref'] == 'Cash' and items['p_amount'] or 0
            sheet.write(row_value, 0, sl, format3)
            sheet.write(row_value, 1, items['partner'], format3)
            sheet.write(row_value, 2, items['invoice_ref'], format3)
            sheet.write(row_value, 3, invoice_date, format3)
            sheet.write(row_value, 4, payment_date.strftime("%d/%b/%Y"), format3)
            sheet.write(row_value, 5, items['total'], format4)
            sheet.write(row_value, 6, cash_payment, format3)
            sheet.write(row_value, 7, bank_payment, format3)
            sheet.write(row_value, 8, items['amount_residual'], format4)
            if items['invoice_ref'] not in list_invoice:
                total += items['total']
                list_invoice.append(items['invoice_ref'])
                balance += items['amount_residual']
            cash_total += cash_payment
            bank_total += bank_payment
            row_value += 1
            sl += 1
        sheet.merge_range(row_value, 0, row_value, 4, "Total ", format1)
        sheet.write(row_value, 5, total, format4)
        sheet.write(row_value, 6, cash_total, format4)
        sheet.write(row_value, 7, bank_total, format4)
        sheet.write(row_value, 8, balance, format4)
