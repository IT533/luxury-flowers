# -*- coding: utf-8 -*-
{
    'name': 'Collection Reports',
    'version': '11.0.1.0.0',
    'summary': """Collection Report XLS and PDF""",
    'author': 'Cybrosys Techno Solutions',
    'company': 'Cybrosys Techno Solutions',
    'website': "https://cybrosys.com/",
    'category': 'Accounting',
    'depends': ['account', 'report_xlsx'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/wizard_collection_report.xml',
        'wizard/partner_collection_balance.xml',
        'report/collection_report.xml',
        'report/collection_report_pdf.xml',
        'report/partner_collection_balance.xml',
    ],
    'demo': [],
    'images': ['static/description/banner.jpg'],
    'license': 'LGPL-3',
    'installable': True,
    'application': False
}
