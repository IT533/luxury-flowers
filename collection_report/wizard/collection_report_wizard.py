# -*- coding: utf-8 -*-
from odoo import fields, models, api


class CollectionReport(models.TransientModel):
    _name = 'collection.report'

    partner_ids = fields.Many2one('res.partner', string='Partners')
    user_ids = fields.Many2one('res.users', string='Sales Person')
    date_from = fields.Date(string='Date From')
    date_to = fields.Date(string='Date To', required=True, default=lambda self: fields.Date.today())
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)

    def print_pdf(self):
        data = {
            'partner_ids': self.partner_ids.id,
            'user_ids': self.user_ids.id,
            'user_name': self.user_ids.partner_id.name,
            'date_from': self.date_from,
            'date_to': self.date_to,
            'company_id': self.company_id.id,
        }
        return self.env.ref('collection_report.collection_report_pdf').report_action(self, data=data)

    def print_xls(self):
        return self.env.ref('collection_report.collection_report_xls').report_action(self, data={})
