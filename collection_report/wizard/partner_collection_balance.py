# -*- coding: utf-8 -*-
from datetime import date
from odoo import fields, models, api


class PartnerCollectionBalance(models.TransientModel):
    _name = 'partner.collection.balance'

    partner_ids = fields.Many2many('res.partner', 'collection_partner_rel',  'collection_id', 'partner_id', string='Partners')
    user_id = fields.Many2one('res.users', string='Sales Person')
    date = fields.Date(string='Date From', default=date.today(), required=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)

    def print_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read([])[0]
        return self.env.ref('collection_report.partner_collection_balance').report_action(self, data=data)
