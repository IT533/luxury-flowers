from odoo import api, models, fields


class ProductInherit(models.Model):
    _inherit = "product.product"

    last_purchase_price = fields.Float(compute='_compute_last_purchase_price', string="Last Purchase Price")
    purchase_currency_id = fields.Many2one('res.currency', compute='_compute_last_purchase_price', string="Currency")

    def _compute_last_purchase_price(self):
        for rec in self:
            last_purchase_order_line = self.env['purchase.order.line'].search([
                ('price_unit', '>', 0),
                ('product_id', '=', rec.id)], limit=1, order="create_date desc")
            rec.last_purchase_price = last_purchase_order_line.price_unit
            rec.purchase_currency_id = last_purchase_order_line.currency_id.id

