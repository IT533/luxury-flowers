# -*- coding: utf-8 -*-
{
    'name': 'Aged Partner Balance',
    'version': '11.0.1.0.0',
    'summary': """Aged Partner Balance Report XLS and PDF""",
    'author': 'Cybrosys Techno Solutions',
    'company': 'Cybrosys Techno Solutions',
    'website': "https://cybrosys.com/",
    'category': 'Accounting',
    'depends': ['account', 'report_xlsx','base_accounting_kit'],
    'data': [
        'report/reports.xml',
        'wizard/wizard_aged_partner_report.xml',
        'report/aged_partner_pdf.xml',
        'views/supplier_aging_report_view.xml',
        'views/credit_limit.xml',
    ],
    'demo': [],
    'images': [],
    'license': 'LGPL-3',
    'installable': True,
    'application': False
}
