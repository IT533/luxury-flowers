# -*- coding: utf-8 -*-
from odoo import fields, models, api


class AgedPartnerReport(models.TransientModel):
    _inherit = 'account.aged.trial.balance'

    user_ids = fields.Many2many('res.users', 'aged_user_rel', 'aged_id', 'user_id', string='Sales Person')
    user_wise_report = fields.Boolean(string='Sales person wise')
    supplier_currency = fields.Boolean(string='Supplier Currency')
    inter_branch = fields.Boolean(string='Inter Branch Receivables', default=False)
    company_id = fields.Many2one('res.company', string='Company')
    partner_ids = fields.Many2many('res.partner', string='Customer / Supplier')
    supplier_type = fields.Selection([('international', 'International Supplier'),
                                      ('local', 'Local Supplier')], string='Supplier Type')
    partner_type = fields.Selection([('other', 'Non Trade'),
                                     ('trade', 'Trade'),
                                     ('inter', 'Inter Branch'),
                                     ('bad_debt', 'Bad Debt')], string='Partner Type', default='trade')
    not_due = fields.Boolean(string="Not Due")
    all_trans = fields.Boolean(string="Exclude paid")

    def _print_report(self, data):
        res = super(AgedPartnerReport, self)._print_report(data)
        data['form'].update(self.read(['user_ids'])[0])
        data['form'].update(self.read(['user_wise_report'])[0])
        data['form'].update(self.read(['supplier_currency'])[0])
        data['form'].update(self.read(['inter_branch'])[0])
        data['form'].update(self.read(['supplier_type']))
        data['form'].update(self.read(['partner_ids'])[0])
        data['form'].update(self.read(['partner_type'])[0])
        data['form'].update(self.read(['all_trans'])[0])
        data['form'].update(self.read(['not_due'])[0])
        if self.supplier_currency and self.result_selection == 'supplier':
            return self.env.ref('custom_aged_partner_balance.action_report_aged_partner_currency').with_context(landscape=True).report_action(self, data=data)
        else:
            return res

    def print_xls(self):
        if self.supplier_currency and self.result_selection == 'supplier':
            return self.env.ref('custom_aged_partner_balance.aged_currency_xlsx').report_action(self, data={})
        elif self.result_selection == 'supplier'and not self.supplier_currency:
            return self.env.ref('custom_aged_partner_balance.aged_payable_report_xlsx').report_action(self, data={})
        else:
            return self.env.ref('custom_aged_partner_balance.aged_partner_xlsx').report_action(self, data={})
