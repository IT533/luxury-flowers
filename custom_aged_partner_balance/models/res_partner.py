# -*- coding: utf-8 -*-

from odoo import fields, models


class ResPartnerExtend(models.Model):
    _inherit = 'res.partner'

    partner_credit_limit = fields.Monetary(string='Credit Limit')
