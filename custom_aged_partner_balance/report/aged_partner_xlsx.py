# -*- coding: utf-8 -*-
import logging
from odoo import models
from datetime import date, datetime


class AgedPartnerReportXlsx(models.AbstractModel):
    _name = "report.custom_aged_partner_balance.aged_partner_report_xls"
    _logger = logging.getLogger(__name__)
    try:
        _inherit = 'report.report_xlsx.abstract'
    except ImportError:
        _logger.debug('Cannot find report_xlsx module for version 11')

    def generate_xlsx_report(self, workbook, obj, vals):
        env_obj = self.env['report.base_accounting_kit.report_agedpartnerbalance']
        result_selection = vals.result_selection
        if result_selection == 'customer':
            account_type = ['receivable']
            account_type_name = "Receivable Accounts"
        elif result_selection == 'supplier':
            account_type = ['payable']
            account_type_name = "Payable Accounts"
        else:
            account_type = ['payable', 'receivable']
            account_type_name = "Receivable and Payable Accounts"
        sales_person_wise = vals.user_wise_report
        date_from = vals.date_from
        target_move = vals.target_move
        if target_move == 'all':
            target_move_name = "All Entries"
        else:
            target_move_name = "All Posted Entries"
        if vals.partner_type == 'trade':
            partner_type = ['Trade Receivable', 'Trade Payable']
        elif vals.partner_type == 'inter':
            partner_type = ['Inter Branch Receivables', 'Inter Branch Payable']
        elif vals.partner_type == 'bad_debt':
            partner_type = ['Trade Receivable-Bad Debt']
        else:
            partner_type = ['Other Receivable (Non-trade)', 'Other Payable']
        period_length = vals.period_length
        user_ids = vals.user_ids.ids
        if sales_person_wise:
            if len(user_ids) < 1:
                user_ids = self.env['res.users'].sudo().search([]).ids
                user_ids.append('')
            user_move_lines = {}
            user_wise_total = {}
            for user in user_ids:
                if user != '':
                    user_name = self.env['res.users'].sudo().browse(user).name
                else:
                    user_name = 'Unknown'
                move_lines, total, dummy = env_obj.with_context({'trans': vals.all_trans}).user_wise_partner_move_lines(account_type, date_from, target_move,
                                                                                period_length, user, partner_type)
                user_move_lines[user_name] = move_lines
                user_wise_total[user_name] = total
        else:
            move_lines, total, dummy = env_obj.with_context({'trans': vals.all_trans, 'company_id': vals.company_id})._get_partner_move_lines(
                account_type, date_from, target_move,
                period_length, [x.id for x in vals.partner_ids],
                partner_type)
        sheet = workbook.add_worksheet()
        format1 = workbook.add_format({'font_size': 16, 'align': 'center',
                                       'bg_color': '#D3D3D3', 'bold': True, 'border': 1})
        format1.set_font_color('#000080')
        format2 = workbook.add_format({'font_size': 10, 'bold': True, 'border': 1})
        format4 = workbook.add_format({'font_size': 10, 'bold': True})
        format3 = workbook.add_format({'font_size': 10, 'border': 1})
        format5 = workbook.add_format({'font_size': 10})
        blue = workbook.add_format({'font_size': 10, 'border': 1, 'bg_color': '#dce6f2'})
        yellow = workbook.add_format({'font_size': 10, 'border': 1, 'bg_color': '#ffff00'})
        green = workbook.add_format({'font_size': 10, 'border': 1, 'bg_color': '#04a204'})
        orange = workbook.add_format({'font_size': 10, 'border': 1, 'bg_color': 'orange'})
        red = workbook.add_format({'font_size': 10, 'border': 1, 'bg_color': '#FF0000'})
        logged_users = self.env['res.company']._company_default_get('account.account')
        sheet.write('A1', logged_users.name, format5)
        sheet.write('A3', 'Start Date:', format4)
        sheet.write('B3', date_from, format5)
        sheet.merge_range('E3:G3', 'Period Length (days):', format4)
        sheet.write('H3', period_length, format5)
        sheet.write('A4', "Partner's:", format4)
        sheet.merge_range('B4:C4', account_type_name, format5)
        sheet.merge_range('E4:F4', 'Target Moves:', format4)
        sheet.merge_range('G4:H4', target_move_name, format5)
        sheet.set_column(0, 0, 20)
        display_date = datetime.strptime(str(date_from), "%Y-%m-%d")
        display_date = display_date.strftime("%d-%b-%Y")
        title = str(logged_users.name).replace("Bloomax", "") + ",Aged " + \
                str(account_type_name).replace("Accounts", "") + \
                "Balance, " + str(display_date)

        if sales_person_wise:
            sheet.set_column(1, 1, 20)
            sheet.merge_range(5, 0, 7, 8, title, format1)
            # sheet.merge_range(5, 0, 7, 8, "Aged Partner Balance", format1)
        else:
            sheet.merge_range(5, 0, 7, 7, title, format1)
            # sheet.merge_range(5, 0, 7, 7, "Aged Partner Balance", format1)
        row_value = 8
        column_value = 0
        if sales_person_wise:
            sheet.write(row_value, column_value, "Sales Person", format2)
            column_value += 1
        sheet.write(row_value, column_value, "Partners", yellow)
        if vals.not_due:
            sheet.write(row_value, column_value + 1, "Not due", yellow)
        elif sales_person_wise:
            column_value = 0
        else:
            column_value = -1
            # sheet.write(row_value, column_value + 1, "", yellow)
        sheet.write(row_value, column_value + 2, "0-" + str(period_length), green)
        sheet.write(row_value, column_value + 3, str(period_length) + "-" + str(2 * period_length), green)
        sheet.write(row_value, column_value + 4, str(2 * period_length) + "-" + str(3 * period_length), orange)
        sheet.write(row_value, column_value + 5, str(3 * period_length) + "-" + str(4 * period_length), red)
        sheet.write(row_value, column_value + 6, "+" + str(4 * period_length), red)
        sheet.write(row_value, column_value + 7, "Total", yellow)
        sheet.write(row_value, column_value + 8, "Remarks", format4)
        row_value += 1
        column_value = 0
        if sales_person_wise:
            for user in user_move_lines:
                if user_move_lines[user]:
                    row_value += 1
                    sheet.write(row_value, column_value, user, format2)
                    sheet.write(row_value, column_value + 1, "", format2)
                    if vals.not_due:
                        sheet.write(row_value, column_value + 2, user_wise_total[user][6], format2)
                        sheet.write(row_value, column_value + 3, user_wise_total[user][4], format2)
                    else:
                        column_value -= 1
                        sheet.write(row_value, column_value + 3, user_wise_total[user][4] + user_wise_total[user][6],
                                    format2)
                    sheet.write(row_value, column_value + 4, user_wise_total[user][3], format2)
                    sheet.write(row_value, column_value + 5, user_wise_total[user][2], format2)
                    sheet.write(row_value, column_value + 6, user_wise_total[user][1], format2)
                    sheet.write(row_value, column_value + 7, user_wise_total[user][0], format2)
                    sheet.write(row_value, column_value + 8, user_wise_total[user][5], format2)
                    column_value += 1
                for partner in user_move_lines[user]:
                    row_value += 1
                    partner_ref = self.env['res.partner'].sudo().browse(partner['partner_id']).ref
                    partner_remark = self.env['res.partner'].sudo().browse(partner['partner_id']).remarks
                    if partner_ref:
                        partner_ref = "[" + str(partner_ref) + "] "
                        partner_name = partner_ref + str(partner['name'])
                    else:
                        partner_name = str(partner['name'])
                    sheet.write(row_value, column_value + 1, partner_name, format3)
                    if vals.not_due:
                        sheet.write(row_value, column_value + 2, partner['direction'], format3)
                        sheet.write(row_value, column_value + 3, partner['4'], format3)
                    else:
                        column_value -= 1
                        sheet.write(row_value, column_value + 3, partner['4'] + partner['direction'], format3)
                    sheet.write(row_value, column_value + 4, partner['3'], format3)
                    sheet.write(row_value, column_value + 5, partner['2'], format3)
                    sheet.write(row_value, column_value + 6, partner['1'], format3)
                    sheet.write(row_value, column_value + 7, partner['0'], format3)
                    sheet.write(row_value, column_value + 8, partner['total'], format3)
                    if partner_remark:
                        sheet.write(row_value, column_value + 9, partner_remark, format3)
                    column_value += 1
            row_value += 1
        elif move_lines:
            sheet.write(row_value, column_value, "Account Total", blue)
            if vals.not_due:
                sheet.write(row_value, column_value + 1, round(total[6], 2), blue)
                sheet.write(row_value, column_value + 2, round(total[4], 2), blue)
            else:
                # sheet.write(row_value, column_value + 1, "", blue)
                column_value = -1
                sheet.write(row_value, column_value + 2, round(total[4], 2) + round(total[6], 2), green)
            # sheet.write(row_value, column_value + 1, round(total[6], 2), format2)
            # sheet.write(row_value, column_value + 2, round(total[4], 2), format2)
            sheet.write(row_value, column_value + 3, round(total[3], 2), green)
            sheet.write(row_value, column_value + 4, round(total[2], 2), orange)
            sheet.write(row_value, column_value + 5, round(total[1], 2), red)
            sheet.write(row_value, column_value + 6, round(total[0], 2), red)
            sheet.write(row_value, column_value + 7, round(total[5], 2), blue)
            row_value += 1
            column_value = 0
            move_lines = sorted(move_lines, key=lambda i: i['total'], reverse=True)
            for i in move_lines:
                partner_ref = self.env['res.partner'].sudo().browse(i['partner_id']).ref
                partner_remark = self.env['res.partner'].sudo().browse(i['partner_id']).remarks
                if partner_ref:
                    partner_ref = "[" + str(partner_ref) + "] "
                    partner_name = partner_ref + str(i['name'])
                else:
                    partner_name = str(i['name'])
                sheet.write(row_value, column_value, partner_name, format3)
                if vals.not_due:
                    sheet.write(row_value, column_value + 1, round(i['direction'], 2), format3)
                    sheet.write(row_value, column_value + 2, round(i['4'], 2), format3)
                else:
                    column_value = -1
                    # sheet.write(row_value, column_value + 1, "", format3)
                    sheet.write(row_value, column_value + 2, round(i['4'], 2) + round(i['direction'], 2), green)
                # sheet.write(row_value, column_value + 1, round(i['direction'], 2), format3)
                # sheet.write(row_value, column_value + 2, round(i['4'], 2), format3)
                sheet.write(row_value, column_value + 3, round(i['3'], 2), green)
                sheet.write(row_value, column_value + 4, round(i['2'], 2), orange)
                sheet.write(row_value, column_value + 5, round(i['1'], 2), red)
                sheet.write(row_value, column_value + 6, round(i['0'], 2), red)
                sheet.write(row_value, column_value + 7, round(i['total'], 2), format3)
                if partner_remark:
                    sheet.write(row_value, column_value + 8, partner_remark, format4)
                column_value = 0
                row_value += 1
