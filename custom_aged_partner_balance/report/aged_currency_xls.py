# -*- coding: utf-8 -*-
import logging
from odoo import models, fields
from datetime import date, datetime


class AgedCurrencyReportXlsx(models.AbstractModel):
    _name = "report.custom_aged_partner_balance.aged_currency_report_xls"
    _logger = logging.getLogger(__name__)
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id)

    try:
        _inherit = 'report.report_xlsx.abstract'
    except ImportError:
        _logger.debug('Cannot find report_xlsx module')

    def generate_xlsx_report(self, workbook, obj, vals):

        env_obj = self.env['report.custom_aged_partner_balance.aging_partnerbalance']
        result_selection = vals.result_selection
        if result_selection == 'customer':
            account_type = ['receivable']
            account_type_name = "Receivable Accounts"
        elif result_selection == 'supplier':
            account_type = ['payable']
            account_type_name = "Payable Accounts"
        else:
            account_type = ['payable', 'receivable']
            account_type_name = "Receivable and Payable Accounts"
        if vals.partner_type == 'trade':
            partner_type = ['Trade Receivable', 'Trade Payable']
        elif vals.partner_type == 'inter':
            partner_type = ['Inter Branch Receivables', 'Inter Branch Payable']
        else:
            partner_type = ['Other Receivable (Non-trade)', 'Other Payable']
        sales_person_wise = vals.user_wise_report
        date_from = vals.date_from
        target_move = vals.target_move
        if target_move == 'all':
            target_move_name = "All Entries"
        else:
            target_move_name = "All Posted Entries"
        period_length = vals.period_length
        move_lines, total, dummy = env_obj.with_context({'company_id': vals.company_id})._get_partner_move_lines(account_type, date_from, target_move,
                                                                   period_length, [x.id for x in vals.partner_ids],
                                                                   partner_type)
        sheet = workbook.add_worksheet()
        format1 = workbook.add_format({'font_size': 16, 'border': 1, 'align': 'center', 'bg_color': '#D3D3D3', 'bold': True})
        grand_format = workbook.add_format({'font_size': 10, 'border': 1, 'align': 'center', 'bg_color': '#D3D3D3', 'bold': True})
        format1.set_font_color('#000080')
        format2 = workbook.add_format({'font_size': 10, 'bold': True})
        format3 = workbook.add_format({'font_size': 10})
        blue = workbook.add_format({'font_size': 10, 'border': 1, 'bg_color': '#dce6f2'})
        yellow = workbook.add_format({'font_size': 10, 'border': 1, 'bg_color': '#ffff00'})
        logged_users = self.env['res.company']._company_default_get('account.account')
        sheet.merge_range('B1:C1', logged_users.name, format3)
        sheet.write('A1', 'BRANCH NAME', format3)
        sheet.write('A3', 'Start Date:', format2)
        sheet.write('B3', date_from, format3)
        sheet.merge_range('E3:G3', 'Period Length (days):', format2)
        sheet.write('H3', period_length, format3)
        sheet.write('A4', "Partner's:", format2)
        sheet.merge_range('B4:C4', account_type_name, format3)
        sheet.merge_range('E4:F4', 'Target Moves:', format2)
        sheet.merge_range('G4:H4', target_move_name, format3)
        sheet.set_column(0, 0, 20)
        display_date = datetime.strptime(str(date_from), "%Y-%m-%d")
        display_date = display_date.strftime("%d-%b-%Y")
        title = str(logged_users.name).replace("Bloomax", "") + ",Aged " + \
                str(account_type_name).replace("Accounts", "") + \
                "Balance, " + str(display_date)
        format2 = workbook.add_format({'font_size': 10, 'bold': True, 'border': 1})
        format3 = workbook.add_format({'font_size': 10, 'border': 1})
        if sales_person_wise:
            sheet.set_column(1, 1, 20)
            sheet.merge_range(5, 0, 7, 9, title, format1)
            # sheet.merge_range(5, 0, 7, 9, "Aged Partner Balance", format1)
        else:
            sheet.merge_range(5, 0, 7, 9, title, format1)
            # sheet.merge_range(5, 0, 7, 8, "Aged Partner Balance", format1)
        row_value = 8
        column_value = 0
        if sales_person_wise:
            sheet.write(row_value, column_value, "Sales Person", format2)
            column_value += 1
        sheet.write(row_value, column_value, "Partners", yellow)
        sheet.write(row_value, column_value + 1, "Credit Limit", yellow)
        sheet.write(row_value, column_value + 2, "Not due", yellow)
        sheet.write(row_value, column_value + 3, "0-" + str(period_length), yellow)
        sheet.write(row_value, column_value + 4, str(period_length) + "-" + str(2 * period_length), yellow)
        sheet.write(row_value, column_value + 5, str(2 * period_length) + "-" + str(3 * period_length), yellow)
        sheet.write(row_value, column_value + 6, str(3 * period_length) + "-" + str(4 * period_length), yellow)
        sheet.write(row_value, column_value + 7, "+" + str(4 * period_length), yellow)
        sheet.write(row_value, column_value + 8, "Total", yellow)
        sheet.write(row_value, column_value + 9, "Total in SR", yellow)
        row_value += 1
        column_value = 0
        total_base = 0.0
        sum_0 = sum([k['0'] for k in move_lines])
        sum_1 = sum([k['1'] for k in move_lines])
        sum_2 = sum([k['2'] for k in move_lines])
        sum_3 = sum([k['3'] for k in move_lines])
        sum_4 = sum([k['4'] for k in move_lines])
        sum_cl = sum([k['credit_limit'] for k in move_lines])
        sum_d = sum([k['direction'] for k in move_lines])
        sum_t = sum([k['total'] for k in move_lines])
        sum_tb = sum([k['total_base'] for k in move_lines])
        sheet.write(row_value, column_value, "Account Total", blue)
        sheet.write(row_value, column_value + 1, sum_cl, blue)
        sheet.write(row_value, column_value + 2, sum_d, blue)
        sheet.write(row_value, column_value + 3, sum_4, blue)
        sheet.write(row_value, column_value + 4, sum_3, blue)
        sheet.write(row_value, column_value + 5, sum_2, blue)
        sheet.write(row_value, column_value + 6, sum_1, blue)
        sheet.write(row_value, column_value + 7, sum_0, blue)
        sheet.write(row_value, column_value + 8, sum_t, blue)
        sheet.write(row_value, column_value + 9, sum_tb, blue)
        row_value += 1
        for i in move_lines:
            partner_ref = self.env['res.partner'].browse(i['partner_id']).ref
            if partner_ref:
                partner_ref = "[" + str(partner_ref) + "] "
                partner_name = partner_ref + str(i['name'])
            else:
                partner_name = str(i['name'])
            sheet.write(row_value, column_value, partner_name, format3)
            sheet.write(row_value, column_value + 1, i['credit_limit'], format3)
            sheet.write(row_value, column_value + 2, i['direction'], format3)
            sheet.write(row_value, column_value + 3, i['4'], format3)
            sheet.write(row_value, column_value + 4, i['3'], format3)
            sheet.write(row_value, column_value + 5, i['2'], format3)
            sheet.write(row_value, column_value + 6, i['1'], format3)
            sheet.write(row_value, column_value + 7, i['0'], format3)
            sheet.write(row_value, column_value + 8, i['total'], format3)
            sheet.write(row_value, column_value + 9, i['total_base'], format3)
            total_base += i['total_base']
            row_value += 1
        sheet.merge_range(row_value, column_value, row_value, column_value + 8, "Grand Total In SAR", grand_format)
        sheet.write(row_value, column_value + 9, total_base, format2)
