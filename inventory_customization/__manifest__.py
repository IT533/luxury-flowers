# -*- coding: utf-8 -*-
{
    "name": "Inventory Customisation",
    "description": """
    Inventory Customisation
    """,

    "depends": [
        'stock'
    ],
    "data": [
        'security/ir.model.access.csv',
        'views/product_farm.xml',
    ],


}

