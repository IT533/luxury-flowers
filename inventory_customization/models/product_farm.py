# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class ProductFarm(models.Model):
    _name = "product.farm"

    name = fields.Char('Name', required=1)
    partner_id = fields.Many2one('res.partner',
                                 required=1,
                                 string="Vendor", domain="[('supplier_rank','>', 0)]")


class StockScrap(models.Model):
    _inherit = 'stock.scrap'

    farm_id = fields.Many2one('product.farm', string="Farm")
    attribute_value_id = fields.Many2one('product.attribute.value',
                                         domain="[('display_type','=', 'color')]",
                                         string="Color")
