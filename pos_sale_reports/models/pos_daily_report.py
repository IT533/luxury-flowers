# -*- coding: utf-8 -*-

from datetime import date

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import datetime


class POSReport(models.TransientModel):
    _name = 'pos.report'

    date_from = fields.Datetime(string="From Date", required=True,
                                default=(datetime.datetime.now() - datetime.timedelta(days=1)).strftime(
                                    '%Y-%m-%d 21:00:00'))
    date_to = fields.Datetime(string="To Date",  default=(datetime.datetime.now() + datetime.timedelta(days=1)).strftime(
                                    '%Y-%m-%d 00:00:00'))
    select_company = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id,
                                     required=True)
    sales_person = fields.Many2one('hr.employee', string='Sales Person')

    @api.constrains('date_from', 'date_to')
    def check_dates(self):
        if self.date_from >= self.date_to:
            raise ValidationError(_('From Date Must Be Anterior ... !'))

    def print_sales_report(self):
        data = {'date_from': self.date_from, 'date_to': self.date_to, 'select_company': self.select_company.id,
                'sales_person': self.sales_person.id}
        return self.env.ref('pos_sale_reports.daily_sales_report').report_action([], data=data)

    def print_sales_report_xls(self):
        data = {'date_from': self.date_from, 'date_to': self.date_to, 'select_company': self.select_company.id,
                'sales_person': self.sales_person.id}
        return self.env.ref('pos_sale_reports.daily_sales_report_xls').report_action(
            self, data=data)
