# -*- coding: utf-8 -*-
{
    "name": "POS Daily Report",
    "version": "11.0.1.0.0",
    "author": "Cybrosys Techno Solutions",
    "category": 'Reporting',
    "company": "Cybrosys Techno Solutions",
    "website": "https://www.cybrosys.com",
    'summary': 'POS Daily Sales Report',
    'description': """""",
    "depends": ['base', 'point_of_sale', 'report_xlsx','hr'],
    "data": [
        "security/ir.model.access.csv",
        "views/pos_report_template.xml",
        "views/pos_report_menu.xml",

    ],
    "installable": True,
    "auto_install": False,
}
