# -*- coding: utf-8 -*-
from datetime import date
import datetime
import pytz
from datetime import datetime, timedelta
from odoo import api, models

fmt = "%d-%b-%Y %H:%M:%S"
fmt1 = "%d-%b-%Y"
timeformat = "%H:%M:%S"


class PosDailySales(models.AbstractModel):
    _name = 'report.pos_sale_reports.report_daily_pos_sales'

    @api.model
    def _get_report_values(self, docids, data=None):
        model = self.env.context.get('active_model')
        docs = self.env[model].browse(self.env.context.get('active_ids', []))
        if data['date_to'] and data['sales_person']:
            query = "(am.date >= %s  and am.date<= %s)and am.company_id = %s and ai.employee_id = %s", \
                    (data['date_from'], data['date_to'], data['select_company'], data['sales_person'])
        lines = []
        invoiced = 0
        not_invoiced = 0
        # cr = self._cr
        if data['date_from'] and data['date_to']:
            pos_orders = self.env['pos.order'].sudo().search([('date_order', '>=', data['date_from']),
                                                              ('date_order', '<=', data['date_to']),
                                                              ('employee_id', '=', data['sales_person'])])
        elif data['date_from'] and not data['date_to']:
            pos_orders = self.env['pos.order'].search([('date_order', '>=', data['date_from']),
                                                       ('employee_id', '=', data['sales_person'])])
        elif not data['date_from'] and data['date_to']:
            pos_orders = self.env['pos.order'].search([('date_order', '<=', data['date_to']),
                                                       ('employee_id', '=', data['sales_person'])])
        else:
            pos_orders = self.env['pos.order'].search([])
        for obj1 in pos_orders:
            if obj1.amount_total < 0:
                net_amount = obj1.amount_total
            else:
                net_amount = obj1.amount_total
            if obj1.account_move:
                invoiced += 1
            else:
                not_invoiced += 1
            user = obj1.employee_id.name
            order_date = obj1.date_order
            user_tz = self.env['res.users'].browse(self.env.uid)
            if user_tz.tz:
                tz = pytz.timezone(user_tz.tz)
                if order_date:
                    db_time = datetime.strptime(str(order_date), "%Y-%m-%d %H:%M:%S")
                    time = pytz.utc.localize(db_time).astimezone(tz).strftime("%Y-%m-%d %H:%M:%S")
                    order_date = datetime.strptime(time, "%Y-%m-%d %H:%M:%S")
            bank_amount = 0
            cash_amount = 0
            stc_amount = 0
            debit = 0
            count = 0
            for line in obj1.lines:
                count += 1
            for statements in obj1.payment_ids:
                if statements.payment_method_id.name == 'Debt':
                    debit = debit + statements.amount
                if statements.payment_method_id.name == "Bank":
                    if statements.amount:
                        bank_amount += statements.amount
                if statements.payment_method_id.name == "Cash":
                    if statements.amount:
                        cash_amount += statements.amount
                if statements.payment_method_id.name == "STC Pay":
                    if statements.amount:
                        stc_amount += statements.amount
            if obj1.partner_id or 1 == 1:
                if obj1.account_move:
                    if obj1.account_move.state in ('open', 'paid', 'posted','invoiced'):
                        vals = {
                            'order': obj1.name,
                            'partner': obj1.partner_id.name,
                            'price': obj1.amount_total,
                            'invoice': obj1.account_move.name,
                            'qty': count,
                            'user': user,
                            'date': order_date,
                            'cash': cash_amount,
                            'bank': bank_amount,
                            'stc': stc_amount,
                            'debit': debit,
                            'net_amount': net_amount,
                            'tax_amount': obj1.amount_tax,
                            'invoiced': invoiced,
                            'not_invoiced': not_invoiced,
                        }
                        lines.append(vals)
                else:
                    vals = {
                        'order': obj1.name,
                        'partner': obj1.partner_id.name,
                        'price': obj1.amount_total,
                        'invoice': obj1.account_move.name,
                        'qty': count,
                        'user': user,
                        'date': order_date,
                        'cash': cash_amount,
                        'bank': bank_amount,
                        'stc': stc_amount,
                        'debit': debit,
                        'net_amount': net_amount,
                        'tax_amount': obj1.amount_tax,
                        'invoiced': invoiced,
                        'not_invoiced': not_invoiced,
                    }
                    lines.append(vals)

        if data['date_from']:
            bill_date = datetime.strptime(data['date_from'], "%Y-%m-%d %H:%M:%S")
            date_from = pytz.utc.localize(bill_date).astimezone(pytz.timezone(self.env.user.tz))
            date_from = date_from.strftime("%d/%b/%Y %H:%M:%S")
        if data['date_to']:
            bill_date = datetime.strptime(data['date_to'], "%Y-%m-%d %H:%M:%S")
            date_to = pytz.utc.localize(bill_date).astimezone(pytz.timezone(self.env.user.tz))
            date_to = date_to.strftime("%d/%b/%Y %H:%M:%S")
            # cr = self._cr

        docargs = {
            'doc_ids': docids,
            'doc_model': model,
            'docs': docs,
            'lines': lines,
            'date_today': date.today().strftime("%d/%b/%Y"),
            'date_from': date_from,
            'date_to': date_to if date_to else date.today().strftime("%d/%b/%Y"),
        }
        return docargs
