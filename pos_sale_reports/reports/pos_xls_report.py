# -*- coding: utf-8 -*-
import logging
import datetime
import pytz
from datetime import datetime, timedelta

from odoo import SUPERUSER_ID, models

fmt = "%d-%b-%Y %H:%M:%S"
fmt1 = "%d-%b-%Y"
timeformat = "%H:%M:%S"


class PosSaleReports(models.AbstractModel):
    _name = 'report.pos_sale_reports.report_daily_pos_sales_xls'
    # _inherit = 'report.report_xlsx.abstract'
    _logger = logging.getLogger(__name__)
    try:
        _inherit = 'report.report_xlsx.abstract'
    except ImportError:
        _logger.debug('Cannot find report_xlsx module for version 11')

    def get_sale_details(self, data):
        model = self.env.context.get('active_model')
        docs = self.env[model].browse(self.env.context.get('active_ids', []))
        lines = []
        invoiced = 0
        not_invoiced = 0
        # cr = self._cr
        if data['date_from'] and data['date_to']:
            pos_orders = self.env['pos.order'].sudo().search([('date_order', '>=', data['date_from']),
                                                              ('date_order', '<=', data['date_to']),
                                                              ('employee_id', '=', data['sales_person'][0])])

        elif data['date_from'] and not data['date_to']:
            pos_orders = self.env['pos.order'].search([('date_order', '>=', data['date_from']),
                                                       ('employee_id', '=', data['sales_person'][0])])
        elif not data['date_from'] and data['date_to']:
            pos_orders = self.env['pos.order'].search([('date_order', '<=', data['date_to']),
                                                       ('employee_id', '=', data['sales_person'][0])])
        else:
            pos_orders = self.env['pos.order'].search([])
        for obj1 in pos_orders:
            if obj1.amount_total < 0:
                net_amount = obj1.amount_total
            else:
                net_amount = obj1.amount_total
            if obj1.account_move:
                invoiced += 1
            else:
                not_invoiced += 1
            user = obj1.employee_id.name
            self.env.cr.execute(
                "select rp.tz as tz from res_partner as rp, res_users as ru where ru.id=%s  and ru.partner_id=rp.id",
                (self.env.uid,))
            tz = self.env.cr.dictfetchall()
            tz = pytz.timezone(tz[0]['tz']) or pytz.utc
            DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
            date_to = pytz.utc.localize(datetime.strptime(str(obj1.date_order), DATETIME_FORMAT)).astimezone(tz)
            order_date = date_to.strftime(fmt)
            # order_date = obj1.date_order
            bank_amount = 0
            cash_amount = 0
            stc_amount = 0
            debit = 0
            count = 0
            for line in obj1.lines:
                count += 1
            for statements in obj1.payment_ids:
                if statements.payment_method_id.name == 'Debt':
                    debit = debit + statements.amount
                if statements.payment_method_id.name == "Bank":
                    if statements.amount:
                        bank_amount += statements.amount
                if statements.payment_method_id.name == "Cash":
                    if statements.amount:
                        cash_amount += statements.amount
                if statements.payment_method_id.name == "STC Pay":
                    if statements.amount:
                        stc_amount += statements.amount
            if obj1.partner_id or 1 == 1:
                if obj1.account_move:
                    if obj1.account_move.state in ('open', 'paid', 'posted', 'invoiced'):
                        vals = {
                            'order': obj1.name,
                            'partner': obj1.partner_id.name,
                            'price': obj1.amount_total,
                            'invoice': obj1.account_move.name,
                            'qty': count,
                            'user': user,
                            'date': order_date,
                            'cash': cash_amount,
                            'bank': bank_amount,
                            'stc': stc_amount,
                            'debit': debit,
                            'net_amount': net_amount,
                            'tax_amount': obj1.amount_tax,
                            'invoiced': invoiced,
                            'not_invoiced': not_invoiced,
                        }
                        lines.append(vals)
                else:
                    vals = {
                        'order': obj1.name,
                        'partner': obj1.partner_id.name,
                        'price': obj1.amount_total,
                        'invoice': obj1.account_move.name,
                        'qty': count,
                        'user': user,
                        'date': order_date,
                        'cash': cash_amount,
                        'bank': bank_amount,
                        'stc': stc_amount,
                        'debit': debit,
                        'net_amount': net_amount,
                        'tax_amount': obj1.amount_tax,
                        'invoiced': invoiced,
                        'not_invoiced': not_invoiced,
                    }
                    lines.append(vals)
        return lines

    def generate_xlsx_report(self, workbook, data, lines):
        model_data = lines.read([])[0]
        date_from = ''
        date_to = ''
        user_tz = self.env['res.users'].browse(self.env.uid)
        if lines.date_from:
            # bill_date = datetime.strptime(lines.date_from, "%Y-%m-%d %H:%M:%S").date()
            bill_date = lines.date_from
            if user_tz.tz:
                tz = pytz.timezone(user_tz.tz)
                if bill_date:
                    db_time = datetime.strptime(str(bill_date), "%Y-%m-%d %H:%M:%S")
                    time = pytz.utc.localize(db_time).astimezone(tz).strftime("%Y-%m-%d %H:%M:%S")
                    date_from = str(datetime.strptime(time, "%Y-%m-%d %H:%M:%S"))
        if lines.date_to:
            # bill_date = datetime.strptime(lines.date_to, "%Y-%m-%d %H:%M:%S").date()
            bill_date = lines.date_to
            if user_tz.tz:
                tz = pytz.timezone(user_tz.tz)
                if bill_date:
                    db_time = datetime.strptime(str(bill_date), "%Y-%m-%d %H:%M:%S")
                    time = pytz.utc.localize(db_time).astimezone(tz).strftime("%Y-%m-%d %H:%M:%S")
                    date_to = str(datetime.strptime(time, "%Y-%m-%d %H:%M:%S"))
        # user_tz = self.env.user.tz or pytz.utc
        # local = pytz.timezone(user_tz)
        # date_from = datetime.strftime(
        #     (pytz.utc.localize(datetime.strptime(lines.date_from, "%Y-%m-%d %H:%M:%S").replace(tzinfo=None)).astimezone(
        #         local)) + timedelta(hours=2, minutes=30), "%Y-%m-%d %H:%M:%S")
        # date_to = datetime.strftime(
        #     (pytz.utc.localize(datetime.strptime(lines.date_to, "%Y-%m-%d %H:%M:%S").replace(tzinfo=None)).astimezone(
        #         local)) + timedelta(hours=2, minutes=30), "%Y-%m-%d %H:%M:%S")
        sheet = workbook.add_worksheet()
        format1 = workbook.add_format({'font_size': 16, 'align': 'vcenter', 'bg_color': '#D3D3D3', 'bold': True})
        format5 = workbook.add_format({'font_size': 10, 'bg_color': '#FFFFFF', 'bold': True})
        format6 = workbook.add_format({'font_size': 10, 'bold': True})
        format7 = workbook.add_format({'font_size': 12, 'bold': True})
        format8 = workbook.add_format({'font_size': 10, })
        justify = workbook.add_format({'right': True, 'left': True, 'font_size': 12})
        format5.set_align('center')
        format6.set_align('center')
        justify.set_align('justify')
        format1.set_align('center')
        if date_from:
            sheet.write('L4', "From Date:", format8)
            sheet.write('M4', date_from, format8)
        else:
            sheet.write('L4', '', format8)
            sheet.write('M4', '', format8)

        if date_to:
            sheet.write('L5', 'To Date:', format8)
            sheet.write('M5', date_to, format8)
        else:
            sheet.write('L5', '', format8)
            sheet.write('M5', '', format8)
        irow = 5
        icol = 0
        sheet.merge_range(0, icol, 2, icol + 13, 'DAILY SALES REPORT', format1)
        sheet.write('A7', "SL.NO", format7)
        sheet.write('B7', "SALES PERSON", format7)
        sheet.write('C7', "INVOICE NO", format7)
        sheet.write('D7', "ORDER", format7)
        sheet.write('E7', "QUANTITY", format7)
        sheet.write('F7', "DATE", format7)
        sheet.write('G7', "Customer Code", format7)
        sheet.merge_range('H7:I7', "CUSTOMER", format7)
        sheet.write('J7', "GRAND TOTAL", format7)
        sheet.write('L7', "TOTAL", format7)
        sheet.merge_range('M7:O7', "PAYMENTS", format7)
        sheet.write('M8', "CASH", format7)
        sheet.write('N8', "BANK", format7)
        sheet.write('O8', "DEBT", format7)
        sheet.write('P8', "STC PAY", format7)
        irow = 8
        price = 0
        cash = 0
        bank = 0
        debt = 0
        stc = 0
        tot = 0
        count = 1
        for i in self.get_sale_details(model_data):
            sheet.write(irow, 0, count, format8)
            sheet.write(irow, 1, i['user'], format8)
            sheet.write(irow, 2, i['invoice'], format8)
            sheet.write(irow, 3, i['order'], format8)
            sheet.write(irow, 4, i['qty'], format8)
            sheet.write(irow, 5, i['date'], format8)
            sheet.merge_range(irow, 7, irow, 8, i['partner'], format8)
            sheet.write(irow, 9, i['price'], format8)
            sheet.write(irow, 11, i['price'], format8)
            sheet.write(irow, 12, i['cash'], format8)
            sheet.write(irow, 13, i['bank'], format8)
            sheet.write(irow, 14, i['debit'], format8)
            sheet.write(irow, 15, i['stc'], format8)
            irow += 1
            count += 1
            price += i['price']
            tot += i['price']
            cash += i['cash']
            bank += i['bank']
            debt += i['debit']
            stc += i['stc']
        sheet.merge_range(irow, 0, irow, 8, "TOTAL", format6)
        sheet.write(irow, 9, price, format6)
        sheet.write(irow, 11, tot, format6)
        sheet.write(irow, 12, cash, format6)
        sheet.write(irow, 13, bank, format6)
        sheet.write(irow, 14, debt, format6)
        sheet.write(irow, 15, stc, format6)
